#punto 1
 
#A
 
mi_Diccionario1={"Primer Valor":1, "Segundo Valor":5,"Tercer Valor":3,"Cuarto Valor":4,"Quinto Valor":5,"Sexto Valor":6, "Septimo Valor":7,"Octavo Valor":8,"Noveno Valor":9,"Decimo Valor":10}
Respuesta=1
for i in mi_Diccionario1:
    Respuesta=Respuesta*mi_Diccionario1[i]
 
    print("Punto 1.A.:","Rpta: ",Respuesta)
 
#B
 
for i in mi_Diccionario1.values():
    contador=0
    for k in mi_Diccionario1.values():
        if k==i:
            contador=contador+1
    if contador>1:
        print(i)
 
#C
 
num=(int(input("Ingrese un número:")))
eliminar=[]
for p in mi_Diccionario1:
    if num == mi_Diccionario1.get(p):
        eliminar.append(p)
    elif num % mi_Diccionario1.get(p)==0:
        eliminar.append(p)
for l in eliminar:
    mi_Diccionario1.pop(l)
print (mi_Diccionario1)    

#punto 2

A={"Item 1":6}
B={"Item 1":4}
def sum_discs (A,B):
    C={}
    for key1,item1 in A.items():
        for key2,item2 in B.items():
            if key1==key2:
                C.setdefault(key1, item1+item2)
                break
    return C
print("Punto 2:", sum_discs(A,B))

#punto 3
 
from itertools import product
Combinacion= { "valor_1": ["a","b"],
          "valor_2": ["c","d"],
          "valor_3": ["e","f"]}
for u in product(*[Combinacion[k] for k in sorted(Combinacion.keys())]):
    print("".join(u))
 